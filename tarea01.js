/* Objeto Grupo:  */


var grupo={
		"clave":"09161140",
		"nombre":"isa",

		"docente":{
						"clave":"123abc",
						"nombre":"Juan",
						"apellidos":"perez",
						"grado_academico":"licenciado"
					},

	    "materias":[ 

					{	
					"clave":"456dfr",
					"nombre":"español"
					},
					{
					"clave":"157wsd",
					"nombre":"matematicas"
					},
					{
					"clave":"980yut",
					"nombre":"ciencias"
					},
					{
					"clave":"075jsh",
					"nombre":"fisica"
					},
					{
					"clave":"928jsk",
					"nombre":"mecanica"
					}
					],

		"alumnos":[
						{
							"clave":"023jdk",
							"nombre":"kisbe",
							"apellidos":"caballero",
							"calificasiones":[
												{	
												"clave_materia":"075jsh",
												"calificasion":"78"
												},
												{
												"clave_materia":"928jsk",
												"calificasion":"90"
												}
												]

						},

						{
							"clave":"023kjd",
							"nombre":"juan",
							"apellidos":"canseco",
							"calificasiones":[
												{	
												"clave_materia":"980yut",
												"calificasion":"100"
												},
												{
												"clave_materia":"157wsd",
												"calificasion":"99"
												}
												]

						},

						{
							"clave":"023asd",
							"nombre":"sarai",
							"apellidos":"lopez",
							"calificasiones":[
												{	
												"clave_materia":"980yut",
												"calificasion":"98"
												},
												{
												"clave_materia":"157wsd",
												"calificasion":"90"
												}
												]

						},
							
						{
							"clave":"023ljs",
							"nombre":"febe",
							"apellidos":"carrasco",
							"calificasiones":[
												{	
												"clave_materia":"980yut",
												"calificasion":"67"
												},
												{
												"clave_materia":"157wsd",
												"calificasion":"89"
												}
												]

						}
					
		}

/**********************************Crud**************************************************************************************************/



